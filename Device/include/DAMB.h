
/* This is device header stub */


#ifndef __DAMB__H__
#define __DAMB__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DAMB.h>


namespace Device
{




class
    DAMB
    : public Base_DAMB
{

public:
    /* sample constructor */
    explicit DAMB ( const Configuration::AMB & config

                    , DRoot * parent

                  ) ;
    /* sample dtr */
    ~DAMB ();




    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DAMB( const DAMB & ) = delete;
    DAMB& operator=(const DAMB &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    void update();
private:
    int m_TempL0T1;
    int m_TempL0T2;

    int m_TempL1T1;
    int m_TempL1T2;

    int m_TempL2T1;
    int m_TempL2T2;

    int m_TempL3T1;
    int m_TempL3T2;

    int m_Dcdc1_temp1;
    int m_Dcdc1_temp2;
    float m_Dcdc1_Vin;
    float m_Dcdc1_Vout;
    float m_Dcdc1_Iout;

    int m_Dcdc2_temp1;
    int m_Dcdc2_temp2;
    float m_Dcdc2_Vin;
    float m_Dcdc2_Vout;
    float m_Dcdc2_Iout;

    int m_Dcdc3_temp1;
    int m_Dcdc3_temp2;
    float m_Dcdc3_Vin;
    float m_Dcdc3_Vout;
    float m_Dcdc3_Iout;

    int m_Dcdc4_temp1;
    int m_Dcdc4_temp2;
    float m_Dcdc4_Vin;
    float m_Dcdc4_Vout;
    float m_Dcdc4_Iout;

    int m_DcdcBarracuda_temp;
    float m_DcdcBarracuda_Vout;
    float m_DcdcBarracuda_Vin;
    float m_DcdcBarracuda_Iout;


};





}

#endif // include guard
