/*
 * FTKlib.h
 *
 *      Author: Ioannis Maznas
 *      email: imaznas@cern.ch
 */



#ifndef FTKLIB_H_
#define FTKLIB_H_

#include <math.h>
#include <unistd.h>
#include <ctime>

//#define DELAY 1E6
#define DELAY_FOR_AUX 5E6 // 5 seconds wait

#include <rcc_error/rcc_error.h>
#include <vme_rcc/vme_rcc.h>




inline bool vmePoke (unsigned int registryAddress,unsigned int ValueToSet) //as defined in VME/TDAQ libraries
{
	static VME_MasterMap_t master_map = {0x0, 0x1000, VME_A32, 0};
	unsigned int ret=987;
	int handle;
	int retries=20;
	while(retries && ret!=VME_SUCCESS)
	{
	   ret = VME_Open();
	   --retries;
	}
	if (ret != VME_SUCCESS)
	{
	    VME_Close();
	    return 0;
	}
	master_map.vmebus_address   = registryAddress;
	master_map.window_size      = 0x10;
	master_map.address_modifier = 2;
	master_map.options          = 0;
	ret = VME_MasterMap(&master_map, &handle);
	if (ret != VME_SUCCESS)
	{
		return 0;
	}
	retries = 20;
	ret = VME_WriteSafeUInt(handle, 0, ValueToSet);
	while(retries && ret!=VME_SUCCESS)
	{
		ret = VME_WriteSafeUInt(handle, 0, ValueToSet);
	   --retries;
	}
	retries = 40;
    if (ret != VME_SUCCESS)
    {
    	ret = VME_MasterUnmap(handle);
    	while(retries && ret!=VME_SUCCESS)
    	{
    		ret = VME_MasterUnmap(handle);
    	   --retries;
    	}
    	VME_Close();
    	return 0;
    }
    ret = VME_MasterUnmap(handle);
    retries = 40;
	while(retries && ret!=VME_SUCCESS)
	{
		ret = VME_MasterUnmap(handle);
	   --retries;
	}
    VME_Close();
    //usleep(DELAY);
    return 1;
}


inline unsigned int vmePeek(unsigned int registryAddress)	//as defined in VME/TDAQ libraries
{
    static VME_MasterMap_t master_map = {0x0, 0x1000, VME_A32, 0};
    int retries=20;
    unsigned int ret=987;
    while(retries && ret!=VME_SUCCESS)
    {
    	ret = VME_Open();
    	--retries;
    }
    int handle;
    unsigned int vmedata;
    if (ret != VME_SUCCESS)
    {
    	ret = VME_Close();
    	return 0x04040404;
    }
    master_map.vmebus_address   = registryAddress;
    master_map.window_size      = 0x10;
    master_map.address_modifier = 2; //amcode
    master_map.options          = 0;
    ret = VME_MasterMap(&master_map, &handle);

    if (ret != VME_SUCCESS)
    {
      return 0x01010101;
    }
    ret = VME_ReadSafeUInt(handle, 0, &vmedata);
    //VME_MasterUnmap(handle);
    //if(VME_MasterUnmap(handle)!= VME_SUCCESS) VME_MasterMapDump();
    if(VME_MasterUnmap(handle)!= VME_SUCCESS) printf("\t*ERROR: Could not Unmap the handle*\t");
    if (ret != VME_SUCCESS)
    {
      ret = VME_Close();
      return 0x02020202;
    }
    retries=10;
    ret=987;
    while(retries && ret!=VME_SUCCESS)
    {
    	ret = VME_Close();
    	--retries;
    }

    if (ret != VME_SUCCESS)
    {
    	return 0x03030303;
    }
    return vmedata;
}

inline bool VmeBusErrorsExist(unsigned int registryValue)
{
	if(registryValue!=0x01010101 && registryValue!=0x04040404 && registryValue!=0x02020202)
		return 0;
	else return 1;
}

inline unsigned int getSlotRegistry(int slotNumber)	//memory Registers for the VME slots
{
	unsigned int SlotRegister = 0xFFFFFFFF;
	switch (slotNumber)
	{
	case 2:
		SlotRegister = 0x10000000;
		break;
	case 3:
		SlotRegister = 0x18000000;
		break;
	case 4:
		SlotRegister = 0x20000000;
		break;
	case 5:
		SlotRegister = 0x28000000;
		break;
	case 6:
		SlotRegister = 0x30000000;
		break;
	case 7:
		SlotRegister = 0x38000000;
		break;
	case 8:
		SlotRegister = 0x40000000;
		break;
	case 9:
		SlotRegister = 0x48000000;
		break;
	case 10:
		SlotRegister = 0x50000000;
		break;
	case 11:
		SlotRegister = 0x58000000;
		break;
	case 12:
		SlotRegister = 0x60000000;
		break;
	case 13:
		SlotRegister = 0x68000000;
		break;
	case 14:
		SlotRegister = 0x70000000;
		break;
	case 15:
		SlotRegister = 0x78000000;
		break;
	case 16:
		SlotRegister = 0x80000000;
		break;
	case 17:
		SlotRegister = 0x88000000;
		break;
	case 18:
		SlotRegister = 0x90000000;
		break;
	case 19:
		SlotRegister = 0x98000000;
		break;
	case 20:
		SlotRegister = 0xA0000000;
		break;
	case 21:
		SlotRegister = 0xA8000000;
		break;
	default:
		printf("  *YOU SHOULDN'T SEE THIS:\nSlot number is out of bounds*\t");
		break;
	}
	return SlotRegister;
}

#endif
