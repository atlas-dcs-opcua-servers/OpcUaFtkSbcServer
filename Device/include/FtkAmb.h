//#ifndef FTKAMB_H_
//#define FTKAMB_H_
#pragma once
#include <FTKlib.h>


#define AMB_CONFIGURATION_LOCKDOWN 900 // 15 minutes till lock is released

static int NumberOfBoards=0;
inline bool AmbExists(int slotNumber)
{
	if (VmeBusErrorsExist(vmePeek(getSlotRegistry(slotNumber))))
		return 0;
	else
		return 1;
}



inline bool CheckIfLambExists (unsigned int LambCkeckRegistry, bool *LambExists)
{
	unsigned int regValue=vmePeek(LambCkeckRegistry);
	if(VmeBusErrorsExist(regValue)==0)
	{
		switch ((vmePeek(LambCkeckRegistry)>>16)&0xf)
		{
		case 0:
			LambExists[0]=0;
			LambExists[1]=0;
			LambExists[2]=0;
			LambExists[3]=0;
			return 1;
		case 1:
			LambExists[0]=1;
			LambExists[1]=0;
			LambExists[2]=0;
			LambExists[3]=0;
			return 1;
		case 2:
			LambExists[0]=0;
			LambExists[1]=1;
			LambExists[2]=0;
			LambExists[3]=0;
			return 1;
		case 3:
			LambExists[0]=1;
			LambExists[1]=1;
			LambExists[2]=0;
			LambExists[3]=0;
			return 1;
		case 4:
			LambExists[0]=0;
			LambExists[1]=0;
			LambExists[2]=1;
			LambExists[3]=0;
			return 1;
		case 5:
			LambExists[0]=1;
			LambExists[1]=0;
			LambExists[2]=1;
			LambExists[3]=0;
			return 1;
		case 6:
			LambExists[0]=0;
			LambExists[1]=1;
			LambExists[2]=1;
			LambExists[3]=0;
			return 1;
		case 7:
			LambExists[0]=1;
			LambExists[1]=1;
			LambExists[2]=1;
			LambExists[3]=0;
			return 1;
		case 8:
			LambExists[0]=0;
			LambExists[1]=0;
			LambExists[2]=0;
			LambExists[3]=1;
			return 1;
		case 9:
			LambExists[0]=1;
			LambExists[1]=0;
			LambExists[2]=0;
			LambExists[3]=1;
			return 1;
		case 10:
			LambExists[0]=0;
			LambExists[1]=1;
			LambExists[2]=0;
			LambExists[3]=1;
			return 1;
		case 11:
			LambExists[0]=1;
			LambExists[1]=1;
			LambExists[2]=0;
			LambExists[3]=1;
			return 1;
		case 12:
			LambExists[0]=0;
			LambExists[1]=0;
			LambExists[2]=1;
			LambExists[3]=1;
			return 1;
		case 13:
			LambExists[0]=1;
			LambExists[1]=0;
			LambExists[2]=1;
			LambExists[3]=1;
			return 1;
		case 14:
			LambExists[0]=0;
			LambExists[1]=1;
			LambExists[2]=1;
			LambExists[3]=1;
			return 1;
		case 15:
			LambExists[0]=1;
			LambExists[1]=1;
			LambExists[2]=1;
			LambExists[3]=1;
			return 1;
		default:
			printf("*Something's wrong with the LambCheck*\t");
			LambExists[0]=0;
			LambExists[1]=0;
			LambExists[2]=0;
			LambExists[3]=0;
			return 0;
		}
	}
	return 1;
}

inline unsigned int indirectRead(unsigned int RegisterToRead,unsigned int slotRegister)
{
	vmePoke(slotRegister+0x38,RegisterToRead);
	return vmePeek(slotRegister+0x3C);
}

inline bool allLambsAreProgrammed(unsigned int slotRegister)
{
	unsigned int LAMB_FW_VERSION=0x01010101; // this is the LAMB_FW_VERSION register. For now the LAMB fw version is 0x01 so the answer should be 0x01010101
	unsigned int FwVersion=indirectRead(LAMB_FW_VERSION,slotRegister);

	if(((FwVersion)&0xff) == 0xff) return 0;
	if(((FwVersion>>8)&0xff) == 0xff) return 0;
	if(((FwVersion>>16)&0xff) == 0xff) return 0;
	if(((FwVersion>>24)&0xff) == 0xff) return 0;
	return 1;
}

inline bool CheckForConfigurationVeto(int slot, int &_configurationVetoTimer)
{
	unsigned int slotRegister=getSlotRegistry(slot);
	unsigned int RC_CONFIGURATION_VETO=slotRegister+0x6024; // Unless the board is under RC configuration this value is 0xXXXX0000 else it's 0xXXXXCAFE



	if(vmePeek(RC_CONFIGURATION_VETO)&0xffff) // a veto has been raised
	{
		if (!_configurationVetoTimer) // first time the veto appears
		{
			_configurationVetoTimer = (long long) time(NULL);
			//printf("Time: %d\n", _configurationVetoTimer);
		}
		else
		{
			long long int currentTimeStamp = time(NULL);
			if (currentTimeStamp - _configurationVetoTimer > AMB_CONFIGURATION_LOCKDOWN) return 0; // ignore lockdown
			//printf("\t\tTime till lock bypass: %d\n", (AMB_CONFIGURATION_LOCKDOWN-(currentTimeStamp - _configurationVetoTimer)));
		}

		return 1;
	}
	_configurationVetoTimer = 0; //reset timer

	return 0;
}



inline bool AmbIsConfigured(int slotNumber, int & _configurationVetoTimer) // fw properly loaded
{
	unsigned int slotRegister=getSlotRegistry(slotNumber);
	//unsigned int GTP_ALIGNMENT_STATUS=slotRegister+0x209C; // appears to change on the fly.
	unsigned int STATUS_REGISTER_VME=slotRegister+0x0;	// not sure what its values should be. lately changed to 0x00000000 (from 0xFFFFFFFF)
	unsigned int BANK_CHECK_SUM=slotRegister+0x10; 	// if patter bank is being loaded its value should be 0

	unsigned int LAMB_STATUS=slotRegister+0x6000;

	if(CheckForConfigurationVeto(slotNumber, _configurationVetoTimer)) return 0;
	//unsigned int LAMB_FW_VERSION=0x01010101; // this is the LAMB_FW_VERSION register. For now the LAMB fw version is 0x01 so the answer should be 0x01010101
	//printf("LAMB_FW=0x%08x\n",indirectRead(LAMB_FW_VERSION,slotRegister));
	bool LambExists[4];

	if( vmePeek(BANK_CHECK_SUM)==0) return 0; // pbank is being loaded, skip monitoring
	if(/*vmePeek(GTP_ALIGNMENT_STATUS)==0x0000fde7 && vmePeek(ROAD_RESETDONE_STATUS)==0xffffffff && */vmePeek(STATUS_REGISTER_VME)==0x00000000 && CheckIfLambExists(LAMB_STATUS, LambExists) && allLambsAreProgrammed(slotRegister) )
		return 1;
	return 0;
}

inline void AmbFalseAlarm(int* AmbTemp)
{
	for (int i=0;i<8;++i)
		AmbTemp[i]=9;
}

// AmbOverheatIsReal essentially checks whether the AMB has been configured by RunControl.
inline bool AmbOverheatIsReal(int slotNumber, int* AmbTemp, int & _configurationVetoTimer)
{
	unsigned int slotRegister=getSlotRegistry(slotNumber);
	unsigned int ROAD_RESETDONE_STATUS=slotRegister+0x40D8;	// should be 0xffffffff when AMB is configured by RunControl
	if(AmbIsConfigured(slotNumber, _configurationVetoTimer) && vmePeek(ROAD_RESETDONE_STATUS)==0xffffffff) return 1;
	return 0;
}

inline bool checkIfAmbOverheat(int slotNumber, int* AmbTemp, int warningTemp, int & _configurationVetoTimer)
{
	bool overheat=0;
	for (int i=0;i<8;++i)
	{
		if(AmbTemp[i]>warningTemp) overheat=1;
	}
	if (!AmbOverheatIsReal(slotNumber,AmbTemp,_configurationVetoTimer))
	{
		overheat=0;
		AmbFalseAlarm(AmbTemp);
	}
	return overheat;
}

inline void getAmbTemps (int slotNumber, int *AmbTemps)
{
	for(int i=0;i<8;++i) AmbTemps[i]=1;
	if(AmbExists(slotNumber))
	{
		bool *LambExists=new bool [4];
		CheckIfLambExists(getSlotRegistry(slotNumber)+0x6000,LambExists);
		unsigned int temp1=vmePeek(getSlotRegistry(slotNumber)+0x30);
		unsigned int temp2=vmePeek(getSlotRegistry(slotNumber)+0x34);
		if(LambExists[0])
		{
			AmbTemps[0]=temp1&0xff;			// tempL0T1
			AmbTemps[1]=temp2&0xff;			// tempL0T2
		}
		//else printf("*WARNING: LAMB0 is missing*  ");
		if(LambExists[1])
		{
			AmbTemps[2]=(temp1>>8)&0xff;	// tempL1T1
			AmbTemps[3]=(temp2>>8)&0xff;	// tempL1T2
		}
		//else printf("*WARNING: LAMB1 is missing*  ");
		if(LambExists[2])
		{
			AmbTemps[4]=(temp1>>16)&0xff;	// tempL2T1
			AmbTemps[5]=(temp2>>16)&0xff;	// tempL2T2
		}
		//else printf("*WARNING: LAMB2 is missing*  ");
		if(LambExists[3])
		{
			AmbTemps[6]=(temp1>>24)&0xff;	// tempL3T1
			AmbTemps[7]=(temp2>>24)&0xff;	// tempL3T2
		}
		//else printf("*WARNING: LAMB3 is missing*  ");

		delete []LambExists;
	}
	for (int i=0;i<8;++i)
		if (AmbTemps[i]>130) AmbTemps[i]=3;
}

inline int checkIfComplement(int registryValue, int numberOfBits)
{
	if (registryValue>>(numberOfBits-1) & 0x1)
	{
		if(numberOfBits==11)
		{
			return -(((~registryValue)&0x7ff) + 1);
		}
		else if(numberOfBits==5)
		{
			return -(((~registryValue)&0x1f) + 1);
		}
		else if(numberOfBits==16)
		{
			return -(((~registryValue)&0xffff) +1);
		}
	}
	else return registryValue;
	return 0;
}

inline float getDcDcParameter(unsigned int registryValue,int expPower,bool withExpPart,bool checkForComplement)
{
	if (withExpPart) //0XFFFFF00000000000 exp and 0x00000FFFFFFFFFFF mantissa (16bit in total) - two's complement
	{
		int mantissa = (registryValue&0x7ff); //mantissa is the 11 less significant bits
		int exponentialPower = (registryValue>>11)&0x1f;	//exponential power is the 5 more significant bits
		return checkIfComplement(mantissa,11)*pow(2,checkIfComplement(exponentialPower,5));
	}
	else if(checkForComplement) //16bit mantissa two's complement
	{
		return checkIfComplement(registryValue&0xffff,16)*pow(2,expPower);
	}
	else
	{
		return (int)registryValue*pow(2,expPower);
	}
}

inline void getDcDcValues (int slotNumber, float **DcDcGigaLynx, float *DcDcBarracuda)
{
	unsigned int pokingRegistry = getSlotRegistry(slotNumber)+0x2160;
	unsigned int peekingRegistry = getSlotRegistry(slotNumber)+0x2164;
	unsigned int * pokingValue = new unsigned int [4];
	pokingValue[0]=0x1000;	//LAMB0
	pokingValue[1]=0xe00;	//LAMB1
	pokingValue[2]=0x1100;	//LAMB2
	pokingValue[3]=0xf00;	//LAMB3

	//____________ DC-DC GigaLynx____________________
	float IoutAD,IoutGAIN,IoutOffSet;
	bool NoError=1;
	for (int i=0;i<4;++i) //loop over DcDcGigaLynx
	{
		//get VinL
		NoError=vmePoke(pokingRegistry,pokingValue[i]+0x88);
		if(NoError)
		{
			DcDcGigaLynx[i][0] = getDcDcParameter(vmePeek(peekingRegistry)&0xffff,0,1,1);
		}

		//get VoutL
		NoError=vmePoke(pokingRegistry,pokingValue[i]+0x8b);
		if(NoError)
		{
			DcDcGigaLynx[i][1] = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),-13,0,1);
		}
		//get Iout
		NoError=vmePoke(pokingRegistry,pokingValue[i]+0x8c);
		if(NoError)
		{

			IoutAD = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),0,1,1);
			NoError=vmePoke(pokingRegistry,pokingValue[i]+0x38);
			if(NoError)
			{
				IoutGAIN = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),0,1,1);
				NoError=vmePoke(pokingRegistry,pokingValue[i]+0x38);
				if(NoError)
				{
					IoutOffSet = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),0,1,1);
					DcDcGigaLynx[i][2]=IoutGAIN*IoutAD+IoutOffSet;
				}
			}
		}
		//get temp1
		NoError=vmePoke(pokingRegistry,pokingValue[i]+0x8d);
		if(NoError)
		{
			DcDcGigaLynx[i][3] = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),0,1,1);
		}
		//get temp2
		NoError=vmePoke(pokingRegistry,pokingValue[i]+0x8e);
		if(NoError)
		{
			DcDcGigaLynx[i][4] = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),0,1,1);
		}
	}
	//____________ DC-DC Barracuda____________________
	float VAD,VOffset,VGain;
	// get	Vin
	NoError=vmePoke(pokingRegistry,0x2988);
	if(NoError)
	{
		VAD = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),0,1,1);
		NoError=vmePoke(pokingRegistry,0x29d4);
		if (NoError)
		{
			VOffset = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),0,1,1);
			NoError=vmePoke(pokingRegistry,0x29d3);
			if(NoError)
			{
				VGain = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),0,0,0);
				DcDcBarracuda[0]=VAD*VGain/8192.+VOffset;
			}
		}
	}
	// get Vout
	NoError=vmePoke(pokingRegistry,0x298b);
	if(NoError)
	{
		VAD = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),-12,0,0);
		NoError=vmePoke(pokingRegistry,0x29d2);
		if (NoError)
		{
			VOffset = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),-12,0,1);
			DcDcBarracuda[1]=VAD+VOffset;
		}
	}
	// get Iout
	NoError=vmePoke(pokingRegistry,0x298c);
	if(NoError)
	{
		IoutAD = getDcDcParameter((vmePeek(peekingRegistry)&0xfffff),0,1,1);
		NoError=vmePoke(pokingRegistry,0x29d6);
		if(NoError)
		{
			IoutGAIN = getDcDcParameter((vmePeek(peekingRegistry)&0xfffff),0,0,0);
			NoError=vmePoke(pokingRegistry,0x29d7);
			if(NoError)
			{
				IoutOffSet = getDcDcParameter((vmePeek(peekingRegistry)&0xfffff),0,1,1);
				DcDcBarracuda[2]=IoutGAIN*IoutAD/8192+IoutOffSet;
			}
		}
	}
	// get Temp
	NoError=vmePoke(pokingRegistry,0x298d);
	if(NoError)
	{
		DcDcBarracuda[3] = getDcDcParameter((vmePeek(peekingRegistry)&0xffff),0,1,1);
	}
	delete [] pokingValue;
}



//#endif
