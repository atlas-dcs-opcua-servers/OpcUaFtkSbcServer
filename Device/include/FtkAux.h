#ifndef FTKAUX_H_
#define FTKAUX_H_

#include <FtkAmb.h>


bool AuxExists(int slotNumber)
{
	if (VmeBusErrorsExist(vmePeek(getSlotRegistry(slotNumber)+0x1000000)))
		return 0;
	else
		return 1;
}


void getAuxTemps (int slotNumber, int *AuxTemps)
{
	for (int i=0;i<6;++i) AuxTemps[i]=1;
	//if (AuxExists(slotNumber) && AmbIsConfigured(slotNumber))
	if (AuxExists(slotNumber))
	{
		unsigned int temp1 = vmePeek(getSlotRegistry(slotNumber)+0x1000110);
		unsigned int temp2 = vmePeek(getSlotRegistry(slotNumber)+0x2000110);
		unsigned int temp3 = vmePeek(getSlotRegistry(slotNumber)+0x3000110);
		unsigned int temp4 = vmePeek(getSlotRegistry(slotNumber)+0x4000110);
		unsigned int temp5 = vmePeek(getSlotRegistry(slotNumber)+0x5000110);
		unsigned int temp6 = vmePeek(getSlotRegistry(slotNumber)+0x6000110);
/*
		AuxTemps[0]=127-0xFF+((temp1>>8)&0xff);		// FPGA1
		AuxTemps[1]=127-0xFF+((temp2>>8)&0xff);		// FPGA2
		AuxTemps[2]=127-0xFF+((temp3>>8)&0xff);		// FPGA3
		AuxTemps[3]=127-0xFF+((temp4>>8)&0xff);		// FPGA4
		AuxTemps[4]=127-0xFF+((temp5>>8)&0xff);		// FPGA5
		AuxTemps[5]=127-0xFF+((temp6>>8)&0xff);		// FPGA6
*/

		//:::::: CHECK IF FIRMWARE PROPERLY LOADED ON FPGAs :::::::
		if((vmePeek(getSlotRegistry(slotNumber)+0x1000000)&0xff)!=0xa1 || (temp1>>7)&0x1!=1) AuxTemps[0]=1;
		else AuxTemps[0]=127-0xFF+((temp1>>8)&0xff);
		if((vmePeek(getSlotRegistry(slotNumber)+0x2000000)&0xff)!=0xa2 || (temp2>>7)&0x1!=1) AuxTemps[1]=1;
		else AuxTemps[1]=127-0xFF+((temp2>>8)&0xff);
		if((vmePeek(getSlotRegistry(slotNumber)+0x3000000)&0xff)!=0xa3 || (temp3>>7)&0x1!=1) AuxTemps[2]=1;
		else AuxTemps[2]=127-0xFF+((temp3>>8)&0xff);
		if((vmePeek(getSlotRegistry(slotNumber)+0x4000000)&0xff)!=0xa4 || (temp4>>7)&0x1!=1) AuxTemps[3]=1;
		else AuxTemps[3]=127-0xFF+((temp4>>8)&0xff);
		if((vmePeek(getSlotRegistry(slotNumber)+0x5000000)&0xff)!=0xa5 || (temp5>>7)&0x1!=1) AuxTemps[4]=1;
		else AuxTemps[4]=127-0xFF+((temp5>>8)&0xff);
		if((vmePeek(getSlotRegistry(slotNumber)+0x6000000)&0xff)!=0xa6 || (temp6>>7)&0x1!=1) AuxTemps[5]=1;
		else AuxTemps[5]=127-0xFF+((temp6>>8)&0xff);
	}
	else if(AuxExists(slotNumber)) // AUX exists but AMB not configured
	{
		AuxTemps[0]=2;
		AuxTemps[1]=2;
		AuxTemps[2]=2;
		AuxTemps[3]=2;
		AuxTemps[4]=2;
		AuxTemps[5]=2;
	}
	else //if AUX does not exist
	{
		AuxTemps[0]=0;
		AuxTemps[1]=0;
		AuxTemps[2]=0;
		AuxTemps[3]=0;
		AuxTemps[4]=0;
		AuxTemps[5]=0;
	}
}



bool checkIfAuxOverheat(int* AuxTemp, int warningTemp)
{
	for (int i=0;i<6;++i)
	{
		if(AuxTemp[i]>warningTemp) return 1;
	}
	return 0;
}

#endif
