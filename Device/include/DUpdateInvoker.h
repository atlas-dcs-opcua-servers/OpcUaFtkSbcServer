/*
 * DUpdateInvoker.h
 *
 *  Created on: Nov 23, 2015
 *      Author: root
 */

#ifndef DUPDATEINVOKER_H_
#define DUPDATEINVOKER_H_
#include <uathread.h>
namespace Device
{

class DUpdateInvoker : public UaThread
{
public:
    DUpdateInvoker ();

    void run ();
    void exit () { m_exitFlag=1; }
private:
    volatile bool m_exitFlag;

};
}
#endif
