
/* This is device header stub */


#ifndef __DAUX__H__
#define __DAUX__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DAUX.h>


namespace Device
{




class
    DAUX
    : public Base_DAUX
{

public:
    /* sample constructor */
    explicit DAUX ( const Configuration::AUX & config

                    , DRoot * parent

                  ) ;
    /* sample dtr */
    ~DAUX ();




    /* delegators for
    cachevariables and sourcevariables */


private:
    /* Delete copy constructor and assignment operator */
    DAUX( const DAUX & ) = delete;
    DAUX& operator=(const DAUX &other) = delete;

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    void update();
private:
    int m_Temp1;
    int m_Temp2;
    int m_Temp3;
    int m_Temp4;
    int m_Temp5;
    int m_Temp6;
};





}

#endif // include guard
