
/* This is device body stub */

//#include <FTKlib.h>
#include <FtkAux.h>
#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DAUX.h>
#include <ASAUX.h>





namespace Device
{




// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111






// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DAUX::DAUX (const Configuration::AUX & config

            , DRoot * parent

           ):
    Base_DAUX( config

               , parent

             ),
/* fill up constructor initialization list here */
			m_Temp1(0),
			m_Temp2(0),
			m_Temp3(0),
			m_Temp4(0),
			m_Temp5(0),
			m_Temp6(0)
{
    /* fill up constructor body here */
}

/* sample dtr */
DAUX::~DAUX ()
{

}

/* delegators for cachevariables and externalvariables */



// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333

void DAUX::update ()
{
	int *AuxTemps =new int[6];
	//int desiredTemperature=100;
	int slotNo;
	if (! m_addressSpaceLink->getSlotNumber(slotNo).isGood())
		m_addressSpaceLink->setSlotNumber(99,OpcUa_Good, UaDateTime::now());
	//    if (! m_addressSpaceLink->getTempL0T1(desiredTemperature).isGood())
	//       return;
	//m_TempL0T1 +=0.01*desiredTemperature;
	//m_addressSpaceLink->setTempL0T1(m_TempL0T1, OpcUa_Good, UaDateTime::now());
	///////////////////////////////////////////////////////////////////////////////

	if(slotNo>1 && slotNo<22)
	{
		if (slotNo>9)printf("\e[2C%d",slotNo);
		else printf("\e[2C %d",slotNo);
		printf("\e[3C");
		if(AuxExists(slotNo))
		{
			getAuxTemps(slotNo,AuxTemps);

			int warningTempValue,criticalTempValue;
			m_addressSpaceLink->getWarningTemperatureLevel(warningTempValue).isGood();
			m_addressSpaceLink->getCriticalTemperatureLevel(criticalTempValue).isGood();
			if(checkIfAuxOverheat(AuxTemps,warningTempValue))
			{
				sleep(5);
				getAuxTemps(slotNo,AuxTemps);
			}
			// :::::::::::::::: DEBUGGING :::::::::::::::::::
			for (int i=0;i<6;++i)
			{
				if(AuxTemps[i]>99)
					printf("\e[1;91m%d \e[m\e[C",AuxTemps[i]);
				else if (AuxTemps[i]>9)
				{
					if (AuxTemps[i]>criticalTempValue)
						printf(" \e[1;91m%d \e[m\e[C",AuxTemps[i]); // bold-bright red-grey bg
					else if (AuxTemps[i]>warningTempValue)
						printf(" \e[1;93;40m%d \e[m\e[C",AuxTemps[i]);
					else
						printf(" %d \e[C",AuxTemps[i]);
				}
				else if (AuxTemps[i]>0)
					printf("  %d \e[C",AuxTemps[i]);
				else
					printf(" ERR\e[C");
			}


    		// :::::::: END OF DEBUGGING :::::::::::::::
			//checkIfAuxOverheatIsReal(AuxTemps,slotNo,warningTempValue);

			m_Temp1=AuxTemps[0];
			m_Temp2=AuxTemps[1];
			m_Temp3=AuxTemps[2];
			m_Temp4=AuxTemps[3];
			m_Temp5=AuxTemps[4];
			m_Temp6=AuxTemps[5];

			m_addressSpaceLink->setTemp1(m_Temp1, OpcUa_Good, UaDateTime::now());
			m_addressSpaceLink->setTemp2(m_Temp2, OpcUa_Good, UaDateTime::now());
			m_addressSpaceLink->setTemp3(m_Temp3, OpcUa_Good, UaDateTime::now());
			m_addressSpaceLink->setTemp4(m_Temp4, OpcUa_Good, UaDateTime::now());
			m_addressSpaceLink->setTemp5(m_Temp5, OpcUa_Good, UaDateTime::now());
			m_addressSpaceLink->setTemp6(m_Temp6, OpcUa_Good, UaDateTime::now());
			//std::cout<<memoryPositions[0]<<std::endl;
			//printf("Slot: %d : %x \t %x \n",slotNo,memoryPositions[0],memoryPositions[1]);

		}
/*		else
		{
    		printf("AUX %d is missing\n",slotNo);
    		UaThread::sleep(1);
    		printf("\e[A"); printf("\r");
		}*/
		/*else
		{
			m_addressSpaceLink->setTemp1(0, OpcUa_Bad, UaDateTime::now());
			m_addressSpaceLink->setTemp2(0, OpcUa_Bad, UaDateTime::now());
			m_addressSpaceLink->setTemp3(0, OpcUa_Bad, UaDateTime::now());
			m_addressSpaceLink->setTemp4(0, OpcUa_Bad, UaDateTime::now());
			m_addressSpaceLink->setTemp5(0, OpcUa_Bad, UaDateTime::now());
			m_addressSpaceLink->setTemp6(0, OpcUa_Bad, UaDateTime::now());
		}*/
		printf("\n");
	}

	delete[] AuxTemps;
}


}


