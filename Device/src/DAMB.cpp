/* This is device body stub */

//#include <FTKlib.h>
#include <FtkAmb.h>
#include <boost/foreach.hpp>

#include <Configuration.hxx>

#include <DAMB.h>
#include <ASAMB.h>

namespace Device {

// 1111111111111111111111111111111111111111111111111111111111111111111111111
// 1     GENERATED CODE STARTS HERE AND FINISHES AT SECTION 2              1
// 1     Users don't modify this code!!!!                                  1
// 1     If you modify this code you may start a fire or a flood somewhere,1
// 1     and some human being may possible cease to exist. You don't want  1
// 1     to be charged with that!                                          1
// 1111111111111111111111111111111111111111111111111111111111111111111111111

// 2222222222222222222222222222222222222222222222222222222222222222222222222
// 2     SEMI CUSTOM CODE STARTS HERE AND FINISHES AT SECTION 3            2
// 2     (code for which only stubs were generated automatically)          2
// 2     You should add the implementation but dont alter the headers      2
// 2     (apart from constructor, in which you should complete initializati2
// 2     on list)                                                          2
// 2222222222222222222222222222222222222222222222222222222222222222222222222

/* sample ctr */
DAMB::DAMB(const Configuration::AMB & config

, DRoot * parent

) :
		Base_DAMB(config

		, parent

		),
		/* fill up constructor initialization list here */
		m_TempL0T1(0), m_TempL0T2(0),

		m_TempL1T1(0), m_TempL1T2(0),

		m_TempL2T1(0), m_TempL2T2(0),

		m_TempL3T1(0), m_TempL3T2(0),

		m_Dcdc1_temp1(0), m_Dcdc1_temp2(0), m_Dcdc1_Vin(0), m_Dcdc1_Vout(0), m_Dcdc1_Iout(
				0),

		m_Dcdc2_temp1(0), m_Dcdc2_temp2(0), m_Dcdc2_Vin(0), m_Dcdc2_Vout(0), m_Dcdc2_Iout(
				0),

		m_Dcdc3_temp1(0), m_Dcdc3_temp2(0), m_Dcdc3_Vin(0), m_Dcdc3_Vout(0), m_Dcdc3_Iout(
				0),

		m_Dcdc4_temp1(0), m_Dcdc4_temp2(0), m_Dcdc4_Vin(0), m_Dcdc4_Vout(0), m_Dcdc4_Iout(
				0),

		m_DcdcBarracuda_temp(0), m_DcdcBarracuda_Vout(0), m_DcdcBarracuda_Iout(
				0), m_DcdcBarracuda_Vin(0) {
	/* fill up constructor body here */
}

/* sample dtr */
DAMB::~DAMB() {
}

/* delegators for cachevariables and externalvariables */

// 3333333333333333333333333333333333333333333333333333333333333333333333333
// 3     FULLY CUSTOM CODE STARTS HERE                                     3
// 3     Below you put bodies for custom methods defined for this class.   3
// 3     You can do whatever you want, but please be decent.               3
// 3333333333333333333333333333333333333333333333333333333333333333333333333
void DAMB::update() {
	int *AmbTemps = new int[8];
	for (int i = 0; i < 8; ++i)
		AmbTemps[i] = 1;
	float *DcDcBarracuda = new float[4];
	float **DcDcGigaLynx = new float*[4];

	// fetch values from the boards
	for (int i = 0; i < 4; i++) {
		DcDcGigaLynx[i] = new float[5];
		DcDcBarracuda[i] = 0;
		for (int j = 0; j < 5; ++j)
			DcDcGigaLynx[i][j] = 0;
	}
	// board parameters must have been received by now

	//int desiredTemperature=100;

	int timestamp = 0;
	//m_addressSpaceLink->setConfigurationVetoTimer(timer+1, OpcUa_Good, UaDateTime::now());
	m_addressSpaceLink->getConfigurationVetoTimer(timestamp).isGood();
	//std::cout<<"Timer: "<<timestamp<<std::endl;

	//	m_addressSpaceLink->getConfigurationVetoTimer(timer+1, OpcUa_Good, UaDateTime::now());

	int slotNo;
	if (!m_addressSpaceLink->getSlotNumber(slotNo).isGood())
		m_addressSpaceLink->setSlotNumber(99, OpcUa_Good, UaDateTime::now());
	int warningTempValue, criticalTempValue;
	m_addressSpaceLink->getWarningTemperatureLevel(warningTempValue).isGood();
	m_addressSpaceLink->getCriticalTemperatureLevel(criticalTempValue).isGood();
//    if (! m_addressSpaceLink->getTempL0T1(desiredTemperature).isGood())
//       return;
	//m_TempL0T1 +=0.01*desiredTemperature;
	//m_addressSpaceLink->setTempL0T1(m_TempL0T1, OpcUa_Good, UaDateTime::now());

	///////////////////////////////////////////////////////////////////////////////
	//printf("\e[2C%d",slotNo); //move one char right and print slot
	//NumberOfBoards++;
	//std::cout<<NumberOfBoards;
	if (slotNo > 1 && slotNo < 22) {
		if (slotNo > 9)
			printf("\e[2C%d", slotNo);
		else
			printf("\e[2C %d", slotNo);

		printf("\e[D\e[4C");

		if (AmbExists(slotNo)) {
			if (AmbIsConfigured(slotNo, timestamp)) {
				getAmbTemps(slotNo, AmbTemps);
				if (checkIfAmbOverheat(slotNo, AmbTemps, criticalTempValue,
						timestamp)) // make sure that no wrong value was fetched during board configuration
						{
					sleep(20);
					getAmbTemps(slotNo, AmbTemps);
				}
				//getDcDcValues(slotNo,DcDcGigaLynx,DcDcBarracuda); //disabled until we find a way to prevent conflicts on PMBUS
				/////////////////////////////////////////////////
				//   instead of reading the actual values from //
				// DC-DC converters, just publish zeros for now//
				/////////////////////////////////////////////////
				/*	for (int i = 0; i < 4; ++i) {
				 for (int j = 0; j < 5; ++j)
				 DcDcGigaLynx[i][j] = 0;
				 DcDcBarracuda[i] = 0;
				 }
				 */

				// :::::::: START PUBLISHING VALUES ::::::::
				m_TempL0T1 = AmbTemps[0];
				m_TempL0T2 = AmbTemps[1];
				m_TempL1T1 = AmbTemps[2];
				m_TempL1T2 = AmbTemps[3];
				m_TempL2T1 = AmbTemps[4];
				m_TempL2T2 = AmbTemps[5];
				m_TempL3T1 = AmbTemps[6];
				m_TempL3T2 = AmbTemps[7];

				m_Dcdc1_Vin = DcDcGigaLynx[0][0];
				m_Dcdc1_Vout = DcDcGigaLynx[0][1];
				m_Dcdc1_Iout = DcDcGigaLynx[0][2];
				m_Dcdc1_temp1 = DcDcGigaLynx[0][3];
				m_Dcdc1_temp2 = DcDcGigaLynx[0][4];

				m_Dcdc2_Vin = DcDcGigaLynx[1][0];
				m_Dcdc2_Vout = DcDcGigaLynx[1][1];
				m_Dcdc2_Iout = DcDcGigaLynx[1][2];
				m_Dcdc2_temp1 = DcDcGigaLynx[1][3];
				m_Dcdc2_temp2 = DcDcGigaLynx[1][4];

				m_Dcdc3_Vin = DcDcGigaLynx[2][0];
				m_Dcdc3_Vout = DcDcGigaLynx[2][1];
				m_Dcdc3_Iout = DcDcGigaLynx[2][2];
				m_Dcdc3_temp1 = DcDcGigaLynx[2][3];
				m_Dcdc3_temp2 = DcDcGigaLynx[2][4];

				m_Dcdc4_Vin = DcDcGigaLynx[3][0];
				m_Dcdc4_Vout = DcDcGigaLynx[3][1];
				m_Dcdc4_Iout = DcDcGigaLynx[3][2];
				m_Dcdc4_temp1 = DcDcGigaLynx[3][3];
				m_Dcdc4_temp2 = DcDcGigaLynx[3][4];

				m_DcdcBarracuda_Vin = DcDcBarracuda[0];
				m_DcdcBarracuda_Vout = DcDcBarracuda[1];
				m_DcdcBarracuda_Iout = DcDcBarracuda[2];
				m_DcdcBarracuda_temp = DcDcBarracuda[3];

				m_addressSpaceLink->setTempL0T1(m_TempL0T1, OpcUa_Good,
						UaDateTime::now());
				m_addressSpaceLink->setTempL0T2(m_TempL0T2, OpcUa_Good,
						UaDateTime::now());
				m_addressSpaceLink->setTempL1T1(m_TempL1T1, OpcUa_Good,
						UaDateTime::now());
				m_addressSpaceLink->setTempL1T2(m_TempL1T2, OpcUa_Good,
						UaDateTime::now());
				m_addressSpaceLink->setTempL2T1(m_TempL2T1, OpcUa_Good,
						UaDateTime::now());
				m_addressSpaceLink->setTempL2T2(m_TempL2T2, OpcUa_Good,
						UaDateTime::now());
				m_addressSpaceLink->setTempL3T1(m_TempL3T1, OpcUa_Good,
						UaDateTime::now());
				m_addressSpaceLink->setTempL3T2(m_TempL3T2, OpcUa_Good,
						UaDateTime::now());

				// Changed OpcUa_Good to OpcUa_Bad as long as DCDC values are not propagated due to PMBUS issues...
				m_addressSpaceLink->setDcdc1_temp1(m_Dcdc1_temp1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc1_temp2(m_Dcdc1_temp2, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc1_Vout(m_Dcdc1_Vout, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc1_Iout(m_Dcdc1_Iout, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc1_Vin(m_Dcdc1_Vin, OpcUa_Bad,
						UaDateTime::now());

				m_addressSpaceLink->setDcdc2_temp1(m_Dcdc2_temp1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc2_temp2(m_Dcdc2_temp2, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc2_Vout(m_Dcdc2_Vout, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc2_Iout(m_Dcdc2_Iout, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc2_Vin(m_Dcdc2_Vin, OpcUa_Bad,
						UaDateTime::now());

				m_addressSpaceLink->setDcdc3_temp1(m_Dcdc3_temp1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc3_temp2(m_Dcdc3_temp2, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc3_Vout(m_Dcdc3_Vout, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc3_Iout(m_Dcdc3_Iout, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc3_Vin(m_Dcdc3_Vin, OpcUa_Bad,
						UaDateTime::now());

				m_addressSpaceLink->setDcdc4_temp1(m_Dcdc4_temp1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc4_temp2(m_Dcdc4_temp2, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc4_Vout(m_Dcdc4_Vout, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc4_Iout(m_Dcdc4_Iout, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc4_Vin(m_Dcdc4_Vin, OpcUa_Bad,
						UaDateTime::now());

				m_addressSpaceLink->setDcdcBarracuda_Vin(m_DcdcBarracuda_Vin,
						OpcUa_Bad, UaDateTime::now());
				m_addressSpaceLink->setDcdcBarracuda_Vout(m_DcdcBarracuda_Vout,
						OpcUa_Bad, UaDateTime::now());
				m_addressSpaceLink->setDcdcBarracuda_Iout(m_DcdcBarracuda_Iout,
						OpcUa_Bad, UaDateTime::now());
				m_addressSpaceLink->setDcdcBarracuda_temp(m_DcdcBarracuda_temp,
						OpcUa_Bad, UaDateTime::now());

			} // if AMB is configured
			else //if not configured
			{
				// :::::::: START PUBLISHING VALUES ::::::::
				m_TempL0T1 = 1;
				m_TempL0T2 = 1;
				m_TempL1T1 = 1;
				m_TempL1T2 = 1;
				m_TempL2T1 = 1;
				m_TempL2T2 = 1;
				m_TempL3T1 = 1;
				m_TempL3T2 = 1;

				//m_Dcdc1_Vin = DcDcGigaLynx[0][0];
				//m_Dcdc1_Vout = DcDcGigaLynx[0][1];
				//m_Dcdc1_Iout = DcDcGigaLynx[0][2];
				m_Dcdc1_temp1 = 1;
				m_Dcdc1_temp2 = 1;

				//m_Dcdc2_Vin = DcDcGigaLynx[1][0];
				//m_Dcdc2_Vout = DcDcGigaLynx[1][1];
				//m_Dcdc2_Iout = DcDcGigaLynx[1][2];
				m_Dcdc2_temp1 = 1;
				m_Dcdc2_temp2 = 1;

				//m_Dcdc3_Vin = DcDcGigaLynx[2][0];
				//m_Dcdc3_Vout = DcDcGigaLynx[2][1];
				//m_Dcdc3_Iout = DcDcGigaLynx[2][2];
				m_Dcdc3_temp1 = 1;
				m_Dcdc3_temp2 = 1;

				//m_Dcdc4_Vin = DcDcGigaLynx[3][0];
				//m_Dcdc4_Vout = DcDcGigaLynx[3][1];
				//m_Dcdc4_Iout = DcDcGigaLynx[3][2];
				m_Dcdc4_temp1 = 1;
				m_Dcdc4_temp2 = 1;

				//m_DcdcBarracuda_Vin = DcDcBarracuda[0];
				//m_DcdcBarracuda_Vout = DcDcBarracuda[1];
				//m_DcdcBarracuda_Iout = DcDcBarracuda[2];
				m_DcdcBarracuda_temp = 1;

				m_addressSpaceLink->setTempL0T1(m_TempL0T1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setTempL0T2(m_TempL0T2, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setTempL1T1(m_TempL1T1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setTempL1T2(m_TempL1T2, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setTempL2T1(m_TempL2T1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setTempL2T2(m_TempL2T2, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setTempL3T1(m_TempL3T1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setTempL3T2(m_TempL3T2, OpcUa_Bad,
						UaDateTime::now());

				// Changed OpcUa_Good to OpcUa_Bad as long as DCDC values are not propagated due to PMBUS issues...
				m_addressSpaceLink->setDcdc1_temp1(m_Dcdc1_temp1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc1_temp2(m_Dcdc1_temp2, OpcUa_Bad,
						UaDateTime::now());
				//m_addressSpaceLink->setDcdc1_Vout(m_Dcdc1_Vout, OpcUa_Bad, UaDateTime::now());
				//m_addressSpaceLink->setDcdc1_Iout(m_Dcdc1_Iout, OpcUa_Bad, UaDateTime::now());
				//m_addressSpaceLink->setDcdc1_Vin(m_Dcdc1_Vin, OpcUa_Bad, UaDateTime::now());

				m_addressSpaceLink->setDcdc2_temp1(m_Dcdc2_temp1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc2_temp2(m_Dcdc2_temp2, OpcUa_Bad,
						UaDateTime::now());
				//m_addressSpaceLink->setDcdc2_Vout(m_Dcdc2_Vout, OpcUa_Bad, UaDateTime::now());
				//m_addressSpaceLink->setDcdc2_Iout(m_Dcdc2_Iout, OpcUa_Bad, UaDateTime::now());
				//m_addressSpaceLink->setDcdc2_Vin(m_Dcdc2_Vin, OpcUa_Bad, UaDateTime::now());

				m_addressSpaceLink->setDcdc3_temp1(m_Dcdc3_temp1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc3_temp2(m_Dcdc3_temp2, OpcUa_Bad,
						UaDateTime::now());
				//m_addressSpaceLink->setDcdc3_Vout(m_Dcdc3_Vout, OpcUa_Bad, UaDateTime::now());
				//m_addressSpaceLink->setDcdc3_Iout(m_Dcdc3_Iout, OpcUa_Bad, UaDateTime::now());
				//m_addressSpaceLink->setDcdc3_Vin(m_Dcdc3_Vin, OpcUa_Bad, UaDateTime::now());

				m_addressSpaceLink->setDcdc4_temp1(m_Dcdc4_temp1, OpcUa_Bad,
						UaDateTime::now());
				m_addressSpaceLink->setDcdc4_temp2(m_Dcdc4_temp2, OpcUa_Bad,
						UaDateTime::now());
				//m_addressSpaceLink->setDcdc4_Vout(m_Dcdc4_Vout, OpcUa_Bad, UaDateTime::now());
				//m_addressSpaceLink->setDcdc4_Iout(m_Dcdc4_Iout, OpcUa_Bad, UaDateTime::now());
				//m_addressSpaceLink->setDcdc4_Vin(m_Dcdc4_Vin, OpcUa_Bad, UaDateTime::now());

				//m_addressSpaceLink->setDcdcBarracuda_Vin(m_DcdcBarracuda_Vin, OpcUa_Bad, UaDateTime::now());
				//m_addressSpaceLink->setDcdcBarracuda_Vout(m_DcdcBarracuda_Vout, OpcUa_Bad, UaDateTime::now());
				//m_addressSpaceLink->setDcdcBarracuda_Iout(m_DcdcBarracuda_Iout, OpcUa_Bad, UaDateTime::now());
				m_addressSpaceLink->setDcdcBarracuda_temp(m_DcdcBarracuda_temp,
						OpcUa_Bad, UaDateTime::now());
			} // if AMB not configured

			m_addressSpaceLink->setConfigurationVetoTimer(timestamp, OpcUa_Good,
					UaDateTime::now());

			/////////////////////////////////////////////////
			// :::::::::::::::: DEBUGGING :::::::::::::::::::
			for (int i = 0; i < 8; ++i) {
				if (AmbTemps[i] != 1) {
					if (AmbTemps[i] > 99)
						printf("\e[1;91;47m%d\e[m\e[2C", AmbTemps[i]); // bold-bright red-grey bg
					else if (AmbTemps[i] > 9) {
						if (AmbTemps[i] > criticalTempValue)
							printf(" \e[1;91m%d\e[m\e[2C", AmbTemps[i]); // bold-bright red-grey bg
						else if (AmbTemps[i] > warningTempValue)
							printf(" \e[1;93;40m%d\e[m\e[2C", AmbTemps[i]);
						else
							printf(" %d\e[2C", AmbTemps[i]);
					} else
						printf("  %d\e[2C", AmbTemps[i]);
				} else {
					printf(" \e[40;97mXX\e[3CXX\e[m\e[2C");
					++i;
				}
			}
			for (int i = 0; i < 4; ++i) {
				//printing DC-DC tempetarures
				if (DcDcGigaLynx[i][3] > 999)
					printf(" INF\e[2C");
				else if (DcDcGigaLynx[i][3] > 99)
					printf(" %d\e[2C", DcDcGigaLynx[i][3]);
				else if (DcDcGigaLynx[i][3] > 9)
					printf(" %1.1f\e[1C", DcDcGigaLynx[i][3]);
				else if (DcDcGigaLynx[i][3] > 0)
					printf(" %1.2f\e[1C", DcDcGigaLynx[i][3]);
				else
					printf("ERROR\e[C");
				if (DcDcGigaLynx[i][4] > 999)
					printf(" INF\e[2C");
				else if (DcDcGigaLynx[i][4] > 99)
					printf(" %d\e[2C", DcDcGigaLynx[i][4]);
				else if (DcDcGigaLynx[i][4] > 9)
					printf(" %1.1f\e[1C", DcDcGigaLynx[i][4]);
				else if (DcDcGigaLynx[i][3] > 0)
					printf(" %1.2f\e[1C", DcDcGigaLynx[i][4]);
				else
					printf("ERROR\e[C");
				//printing DC-DC power parameters
				if (DcDcGigaLynx[i][0] > 999)
					printf(" INF\e[2C");
				else if (DcDcGigaLynx[i][0] > 99)
					printf(" %1.1f\e[2C", DcDcGigaLynx[i][0]);
				else if (DcDcGigaLynx[i][0] > 9)
					printf(" %1.1f\e[C", DcDcGigaLynx[i][0]);
				else if (DcDcGigaLynx[i][0] > 0)
					printf(" %1.2f\e[C", DcDcGigaLynx[i][0]);
				else
					printf("ERROR\e[C");
				if (DcDcGigaLynx[i][1] > 999)
					printf(" INF\e[2C");
				else if (DcDcGigaLynx[i][1] > 99)
					printf(" %1.1f\e[2C", DcDcGigaLynx[i][1]);
				else if (DcDcGigaLynx[i][1] > 9)
					printf(" %1.1f\e[1C", DcDcGigaLynx[i][1]);
				else if (DcDcGigaLynx[i][1] > 0)
					printf(" %1.2f\e[1C", DcDcGigaLynx[i][1]);
				else
					printf("ERROR\e[C");
				if (DcDcGigaLynx[i][2] > 999)
					printf(" INF\e[2C");
				else if (DcDcGigaLynx[i][2] > 99)
					printf(" %1.1f\e[2C", DcDcGigaLynx[i][2]);
				else if (DcDcGigaLynx[i][2] > 9)
					printf(" %1.1f\e[1C", DcDcGigaLynx[i][2]);
				else if (DcDcGigaLynx[i][2] > 0)
					printf(" %1.2f\e[1C", DcDcGigaLynx[i][2]);
				else
					printf("ERROR\e[C");
			}
			//printing DC-DC tempetarures
			if (DcDcBarracuda[3] > 999)
				printf(" INF\e[2C");
			else if (DcDcBarracuda[3] > 99)
				printf(" %d\e[2C", DcDcBarracuda[3]);
			else if (DcDcBarracuda[3] > 9)
				printf(" %1.1f\e[1C", DcDcBarracuda[3]);
			else if (DcDcBarracuda[3] > 0)
				printf(" %1.2f\e[1C", DcDcBarracuda[3]);
			else
				printf("ERROR\e[C");
			//printing DC-DC power parameters
			if (DcDcBarracuda[0] > 999)
				printf(" INF\e[2C");
			else if (DcDcBarracuda[0] > 99)
				printf(" %1.1f\e[2C", DcDcBarracuda[0]);
			else if (DcDcBarracuda[0] > 9)
				printf(" %1.1f\e[C", DcDcBarracuda[0]);
			else if (DcDcBarracuda[0] > 0)
				printf(" %1.2f\e[C", DcDcBarracuda[0]);
			else
				printf("ERROR\e[C");
			if (DcDcBarracuda[1] > 999)
				printf(" INF\e[2C");
			else if (DcDcBarracuda[1] > 99)
				printf(" %1.1f\e[2C", DcDcBarracuda[1]);
			else if (DcDcBarracuda[1] > 9)
				printf(" %1.1f\e[1C", DcDcBarracuda[1]);
			else if (DcDcBarracuda[1] > 0)
				printf(" %1.2f\e[1C", DcDcBarracuda[1]);
			else
				printf("ERROR\e[C");
			if (DcDcBarracuda[2] > 999)
				printf(" INF\e[2C");
			else if (DcDcBarracuda[2] > 99)
				printf(" %1.1f\e[2C", DcDcBarracuda[2]);
			else if (DcDcBarracuda[2] > 9)
				printf(" %1.1f\e[1C", DcDcBarracuda[2]);
			else if (DcDcBarracuda[2] > 0)
				printf(" %1.2f\e[1C", DcDcBarracuda[2]);
			else
				printf("ERROR\e[C");

			// :::::::: END OF DEBUGGING :::::::::::::::

		} // if AMB exists
		else //if AMB does not exist
		{
			// :::::::: START PUBLISHING VALUES ::::::::
			m_TempL0T1 = 0;
			m_TempL0T2 = 0;
			m_TempL1T1 = 0;
			m_TempL1T2 = 0;
			m_TempL2T1 = 0;
			m_TempL2T2 = 0;
			m_TempL3T1 = 0;
			m_TempL3T2 = 0;

			//m_Dcdc1_Vin = DcDcGigaLynx[0][0];
			//m_Dcdc1_Vout = DcDcGigaLynx[0][1];
			//m_Dcdc1_Iout = DcDcGigaLynx[0][2];
			m_Dcdc1_temp1 = 0;
			m_Dcdc1_temp2 = 0;

			//m_Dcdc2_Vin = DcDcGigaLynx[1][0];
			//m_Dcdc2_Vout = DcDcGigaLynx[1][1];
			//m_Dcdc2_Iout = DcDcGigaLynx[1][2];
			m_Dcdc2_temp1 = 0;
			m_Dcdc2_temp2 = 0;

			//m_Dcdc3_Vin = DcDcGigaLynx[2][0];
			//m_Dcdc3_Vout = DcDcGigaLynx[2][1];
			//m_Dcdc3_Iout = DcDcGigaLynx[2][2];
			m_Dcdc3_temp1 = 0;
			m_Dcdc3_temp2 = 0;

			//m_Dcdc4_Vin = DcDcGigaLynx[3][0];
			//m_Dcdc4_Vout = DcDcGigaLynx[3][1];
			//m_Dcdc4_Iout = DcDcGigaLynx[3][2];
			m_Dcdc4_temp1 = 0;
			m_Dcdc4_temp2 = 0;

			//m_DcdcBarracuda_Vin = DcDcBarracuda[0];
			//m_DcdcBarracuda_Vout = DcDcBarracuda[1];
			//m_DcdcBarracuda_Iout = DcDcBarracuda[2];
			m_DcdcBarracuda_temp = 0;

			m_addressSpaceLink->setTempL0T1(m_TempL0T1, OpcUa_Bad,
					UaDateTime::now());
			m_addressSpaceLink->setTempL0T2(m_TempL0T2, OpcUa_Bad,
					UaDateTime::now());
			m_addressSpaceLink->setTempL1T1(m_TempL1T1, OpcUa_Bad,
					UaDateTime::now());
			m_addressSpaceLink->setTempL1T2(m_TempL1T2, OpcUa_Bad,
					UaDateTime::now());
			m_addressSpaceLink->setTempL2T1(m_TempL2T1, OpcUa_Bad,
					UaDateTime::now());
			m_addressSpaceLink->setTempL2T2(m_TempL2T2, OpcUa_Bad,
					UaDateTime::now());
			m_addressSpaceLink->setTempL3T1(m_TempL3T1, OpcUa_Bad,
					UaDateTime::now());
			m_addressSpaceLink->setTempL3T2(m_TempL3T2, OpcUa_Bad,
					UaDateTime::now());

			// Changed OpcUa_Good to OpcUa_Bad as long as DCDC values are not propagated due to PMBUS issues...
			m_addressSpaceLink->setDcdc1_temp1(m_Dcdc1_temp1, OpcUa_Bad,
					UaDateTime::now());
			m_addressSpaceLink->setDcdc1_temp2(m_Dcdc1_temp2, OpcUa_Bad,
					UaDateTime::now());
			//m_addressSpaceLink->setDcdc1_Vout(m_Dcdc1_Vout, OpcUa_Bad, UaDateTime::now());
			//m_addressSpaceLink->setDcdc1_Iout(m_Dcdc1_Iout, OpcUa_Bad, UaDateTime::now());
			//m_addressSpaceLink->setDcdc1_Vin(m_Dcdc1_Vin, OpcUa_Bad, UaDateTime::now());

			m_addressSpaceLink->setDcdc2_temp1(m_Dcdc2_temp1, OpcUa_Bad,
					UaDateTime::now());
			m_addressSpaceLink->setDcdc2_temp2(m_Dcdc2_temp2, OpcUa_Bad,
					UaDateTime::now());
			//m_addressSpaceLink->setDcdc2_Vout(m_Dcdc2_Vout, OpcUa_Bad, UaDateTime::now());
			//m_addressSpaceLink->setDcdc2_Iout(m_Dcdc2_Iout, OpcUa_Bad, UaDateTime::now());
			//m_addressSpaceLink->setDcdc2_Vin(m_Dcdc2_Vin, OpcUa_Bad, UaDateTime::now());

			m_addressSpaceLink->setDcdc3_temp1(m_Dcdc3_temp1, OpcUa_Bad,
					UaDateTime::now());
			m_addressSpaceLink->setDcdc3_temp2(m_Dcdc3_temp2, OpcUa_Bad,
					UaDateTime::now());
			//m_addressSpaceLink->setDcdc3_Vout(m_Dcdc3_Vout, OpcUa_Bad, UaDateTime::now());
			//m_addressSpaceLink->setDcdc3_Iout(m_Dcdc3_Iout, OpcUa_Bad, UaDateTime::now());
			//m_addressSpaceLink->setDcdc3_Vin(m_Dcdc3_Vin, OpcUa_Bad, UaDateTime::now());

			m_addressSpaceLink->setDcdc4_temp1(m_Dcdc4_temp1, OpcUa_Bad,
					UaDateTime::now());
			m_addressSpaceLink->setDcdc4_temp2(m_Dcdc4_temp2, OpcUa_Bad,
					UaDateTime::now());
			//m_addressSpaceLink->setDcdc4_Vout(m_Dcdc4_Vout, OpcUa_Bad, UaDateTime::now());
			//m_addressSpaceLink->setDcdc4_Iout(m_Dcdc4_Iout, OpcUa_Bad, UaDateTime::now());
			//m_addressSpaceLink->setDcdc4_Vin(m_Dcdc4_Vin, OpcUa_Bad, UaDateTime::now());

			//m_addressSpaceLink->setDcdcBarracuda_Vin(m_DcdcBarracuda_Vin, OpcUa_Bad, UaDateTime::now());
			//m_addressSpaceLink->setDcdcBarracuda_Vout(m_DcdcBarracuda_Vout, OpcUa_Bad, UaDateTime::now());
			//m_addressSpaceLink->setDcdcBarracuda_Iout(m_DcdcBarracuda_Iout, OpcUa_Bad, UaDateTime::now());
			m_addressSpaceLink->setDcdcBarracuda_temp(m_DcdcBarracuda_temp,
					OpcUa_Bad, UaDateTime::now());
		} //if AMB does not exist
		/*    	else
		 {
		 printf("AMB %d is missing\n",slotNo);
		 UaThread::sleep(1);
		 printf("\e[A"); printf("\r");

		 //sleep(2);
		 //printf("\e[A"); printf("\r");
		 }*/
		/*    	else //if AMB is missing
		 {
		 m_addressSpaceLink->setTempL0T1(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setTempL0T2(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setTempL1T1(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setTempL1T2(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setTempL2T1(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setTempL2T2(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setTempL3T1(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setTempL3T2(0, OpcUa_Bad, UaDateTime::now());

		 m_addressSpaceLink->setDcdc1_temp1(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc1_temp2(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc1_Vout(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc1_Iout(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc1_Vin(0, OpcUa_Bad, UaDateTime::now());

		 m_addressSpaceLink->setDcdc2_temp1(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc2_temp2(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc2_Vout(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc2_Iout(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc2_Vin(0, OpcUa_Bad, UaDateTime::now());

		 m_addressSpaceLink->setDcdc3_temp1(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc3_temp2(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc3_Vout(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc3_Iout(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc3_Vin(0, OpcUa_Bad, UaDateTime::now());

		 m_addressSpaceLink->setDcdc4_temp1(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc4_temp2(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc4_Vout(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc4_Iout(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdc4_Vin(0, OpcUa_Bad, UaDateTime::now());

		 m_addressSpaceLink->setDcdcBarracuda_Vin(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdcBarracuda_Vout(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdcBarracuda_Iout(0, OpcUa_Bad, UaDateTime::now());
		 m_addressSpaceLink->setDcdcBarracuda_temp(0, OpcUa_Bad, UaDateTime::now());
		 }*/
		//else printf("\r");
		printf("\n");
	}

	//std::cout<<memoryPositions[0]<<std::endl;
	//printf("Slot: %d : %x \t %x \n",slotNo,memoryPositions[0],memoryPositions[1]);

	delete[] AmbTemps;
	delete[] DcDcBarracuda;
	for (int i = 0; i < 4; ++i)
		delete[] DcDcGigaLynx[i];
	delete[] DcDcGigaLynx;
}

}

