#include <iostream>
#include <boost/foreach.hpp>

#include <DRoot.h>
#include <DUpdateInvoker.h>

#include <DAMB.h>
#include <DAUX.h>
#define DELAY 2E6 //interval between scans in microseconds


namespace Device
{

DUpdateInvoker::DUpdateInvoker ():
        m_exitFlag(0)
{
}

void DUpdateInvoker::run()
{
	//system("reset");//
	system("clear");
	//:::::::::::::: table setup :::::::::::::

	printf("\e[%d;3f",2); //move to line 2 (table setup)
	printf("\e[6m\t::::::::: AMB STATUS :::::::::\e[m\n"
			"______________________________________________________________________________________________________________________________________________________________________________________________\n");
	printf("      | LAMB 0  | LAMB 1  | LAMB 2  | LAMB 3  |     DC-DC Converter 1       |     DC-DC Converter 2       |     DC-DC Converter 3       |     DC-DC Converter 4       |     DC-DC Barracuda   |\n");
	printf(" SLOT | T1 | T2 | T1 | T2 | T1 | T2 | T1 | T2 |  T1 |  T2 |  Vi |  Vo |  Io |  T1 |  T2 |  Vi |  Vo |  Io |  T1 |  T2 |  Vi |  Vo |  Io |  T1 |  T2 |  Vi |  Vo |  Io |  T  |  Vi |  Vo |  Io |\n");

	printf("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
	for(int i=0;i<16;++i)
	{
		printf("      |    |    |    |    |    |    |    |    |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |\n");
	}

	printf("\e[%d;0f",24); //move to line 23
    printf("\t\e[5m::::::::: AUX STATUS :::::::::\e[m\n"
            		"____________________________________________________\n");
    printf(" SLOT | T1 | T2 | T3 | T4 | T5 | T6 |\n");
    printf("-------------------------------------\n");
    for(int i=0;i<16;++i)
    {
    	printf("      |    |    |    |    |    |    |\n");
    }

    int i=1;
    while (!m_exitFlag)
    {

        //system("clear");
        printf("\e[%d;1f",1);
        if(i==1)
        {
        	i--;
        	std::cout << "tick" << std::endl;
        }
        else
        {
        	i++;
        	std::cout << "tack" << std::endl;
        }

        printf("\e[%d;0f",7); //start filling AMB table
        BOOST_FOREACH(DAMB* h, DRoot::getInstance()->ambs())
        {
            h->update();
        }

        printf("\e[%d;0f",28); //start filling AUX table
        BOOST_FOREACH(DAUX* h, DRoot::getInstance()->auxs())
        {
            h->update();
        }
        printf("\e[%d;1f",45); //place cursor at the end of the table
        //UaThread::sleep(DELAY);
        usleep(DELAY);
    }
    std::cout << "thread exit" << std::endl;
}

}
